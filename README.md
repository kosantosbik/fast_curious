# To initialize
sh init_worlds.sh

# Source the setup files
source /opt/ros/kinetic/setup.bash

source ~/catkin_ws/devel/setup.bash

# Launch the project with different worlds ( yes we used assignment 1 files as template)
roslaunch a1_456_referee w1.launch

roslaunch a1_456_referee w2.launch

roslaunch a1_456_referee w3.launch
