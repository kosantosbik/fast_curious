#!/usr/bin/python
# -*- coding: utf-8 -*-

# Gürsel Özbek
# 23.12.2018

import rospy
import sys
import roslib
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
from PIL import Image as PILImage
import imutils
import math
from time import sleep

class Capture:

    def __init__(self, cv2_img):

        # Reading Image
        # self.img = cv2.imread(file_name)

        self.img = cv2_img
        self.im_pil = PILImage.fromarray(self.img)
        self.im_pil = self.im_pil.convert('RGB')

    def clear_bg(self):
        self.pixdata = self.im_pil.load()

        # Clean the background.
        for y in xrange(self.im_pil.size[1]):
            for x in xrange(self.im_pil.size[0]):

                (b, g, r) = self.pixdata[x, y]
                if not (b == g and r > 100 + b or g == r and b > 100
                        + r or b == r and g > 100 + r):
                    self.pixdata[x, y] = (0, 0, 0)

        self.im_np = np.asarray(self.im_pil)

    """
    SOME PART OF THE BELOW CODE WHICH CONTAINS FILTERS AND FINDING CONTOURS TAKEN FROM
    https://learndeltax.blogspot.com/2016/05/3d-object-detection-using-opencv-python.html
    """

    def filter(self):

        # RGB to Gray

        img_gray = cv2.cvtColor(self.im_np, cv2.COLOR_RGB2GRAY)

        # Noise removal with iterative bilateral filter

        noise_removal = cv2.bilateralFilter(img_gray, 9, 75, 75)

        # Thresholding the image

        (ret, thresh_image) = cv2.threshold(noise_removal, 0, 255,
                cv2.THRESH_BINARY)

        # Canny Edge detection

        canny_image = cv2.Canny(thresh_image, 250, 255)
        canny_image = cv2.convertScaleAbs(canny_image)

        # dilation to strengthen the edges

        kernel = np.ones((3, 3), np.uint8)

        # Creating the kernel for dilation

        self.dilated_image = cv2.dilate(canny_image, kernel,
                iterations=1)


    def show_filtered(self):
        cv2.imshow('Filtered Image', self.dilated_image)

    def find_contours(self):
        cnts = cv2.findContours(self.dilated_image.copy(),
                                cv2.RETR_EXTERNAL,
                                cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)


        #filters only bigger contours for optimized results 
        cnts = list(filter(lambda x: cv2.contourArea(x)> 2000, cnts))
        cnts= sorted(cnts, key = cv2.contourArea, reverse = True)
        to_publish = []
        
        # loop over the contours
        for c in cnts:

            area = cv2.contourArea(c)

            # center of the contour

            M = cv2.moments(c)
            cX = int(M['m10'] / M['m00'])
            cY = int(M['m01'] / M['m00'])

            color = self.detect_color(cX, cY)
            shape = self.detect_shape(c, area)
            text = color + ' ' + shape
            code = shape+"_"+color.lower()

            self.draw_contours(text,c,cX,cY)
            to_publish.append(code)
        return to_publish

    def draw_contours(self,text,contour,cX,cY):
            cv2.drawContours(self.img, [contour], -1, (0, 255, 0), 2)
            # len(text) * 4 shifting for centering
            cv2.putText(self.img,text,(cX - len(text) * 4, cY),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255),2, )
    	

    def get_image(self):
        return self.img

    def detect_color(self, cx, cy):
        (b, g, r) = self.pixdata[cx, cy]
        if b > g and b > r:
            return 'Blue'
        elif g > b and g > r:
            return 'Green'
        elif r > b and r > g:
            return 'Red'
        else:
            return ''

    # https://docs.opencv.org/3.1.0/dd/d49/tutorial_py_contour_features.html
    # USED AS REFERENCE
    def detect_shape(self, c, area):

        shape = 'unidentified'
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.025 * peri, True)

        #print(len(approx))

        if len(approx) == 3 or len(approx) == 6 or len(approx) == 7:
            shape = 'pyramid'
        elif len(approx) > 8:
            shape = 'star'
        elif len(approx) == 4:
            _,radius = cv2.minEnclosingCircle(approx)
            ratio = radius*radius*math.pi / area
            #print("ratio", ratio)
            shape = "cube" if ratio <= 1.60 else "pyramid"
        elif len(approx) == 5:
            _,radius = cv2.minEnclosingCircle(approx)
            ratio = radius*radius*math.pi / area
            #print("ratio", ratio)
            shape = "cube" if ratio <= 1.60 else "pyramid"
        elif len(approx) == 8:
            shape = 'sphere'
        else:
            shape = str(len(approx))

        return shape


"""
BELOW CODE WHICH CONVERTS RAW IMAGE TO CV2 IMAGE TAKEN FROM 
http://wiki.ros.org/cv_bridge/Tutorials/ConvertingBetweenROSImagesAndOpenCVImagesPython
"""

class ObjectDetector:

    def __init__(self):
        print("Object detector is working...")
        #publish objects as string with delimeter ','
        self.object_pub = rospy.Publisher('detected_objects', String, queue_size=10)

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber('/camera/rgb/image_raw',
                Image, self.callback, queue_size=1)
        self.fps_optimizer = 0

    def callback(self, data):
        if self.fps_optimizer % 12 == 0:
            try:
                cv_image = self.bridge.imgmsg_to_cv2(data, 'bgr8')
            except CvBridgeError, e:
                print(e)

            capture = Capture(cv_image)
            capture.clear_bg()
            capture.filter()
		    #capture.show_filtered()
            to_publish = capture.find_contours()  

            cv2.imshow('Image window', capture.get_image())
            if len(to_publish):
                self.object_pub.publish(','.join(to_publish))
            cv2.waitKey(1)
            
        self.fps_optimizer +=1


## This is the method we initilize everything

def explorer_node():

    rospy.init_node('object_detector', anonymous=True)
    ic = ObjectDetector()

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print('Shutting down')
    cv2.destroyAllWindows()


if __name__ == '__main__':
    explorer_node()

			