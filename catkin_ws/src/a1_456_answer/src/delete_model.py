import subprocess
import os
from pprint import pprint

class ModelDelete():
    def delete_model(self, model_key):
	"Deletes model for given key. Key type: <shape>_<color> . Example: sphere_green"
        model_name = self.models[model_key]
        delete_command = "rosservice call gazebo/delete_model '{model_name: " + model_name + "}'"
        os.system(delete_command)

    def __init__(self):
	"gather the models list from rostopic /gazebo/model_states"
        get_models = subprocess.Popen(["rostopic", "echo", "-n", "1", "/gazebo/model_states"],
                                      stdout=subprocess.PIPE,
                                      stderr=subprocess.STDOUT)
        out, err = get_models.communicate()
        start_index = out.index('[') + 1
        end_index = out.index(']')
        csm =  out[start_index:end_index]
        models_wc = csm.split()
        models_list = []
        self.models = {}
        for model in models_wc:
            if model[-1] == ',':
                models_list.append(model[:-1])
            else:
                models_list.append(model)

        for model in models_list:
            if "sphere" in model:
                if "blue" in model:
                    self.models['sphere_blue'] = model
                elif "red" in model:
                    self.models['sphere_red'] = model
                elif "green" in model:
                    self.models['sphere_green'] = model
                else:
                    continue
            elif "pyramid" in model:
                if "blue" in model:
                    self.models['pyramid_blue'] = model
                elif "red" in model:
                    self.models['pyramid_red'] = model
                elif "green" in model:
                    self.models['pyramid_green'] = model
                else:
                    continue
            elif "star" in model:
                if "blue" in model:
                    self.models['star_blue'] = model
                elif "red" in model:
                    self.models['star_red'] = model
                elif "green" in model:
                    self.models['star_green'] = model
                else:
                    continue
            elif "box" in model:
                if "blue" in model:
                    self.models['cube_blue'] = model
                elif "red" in model:
                    self.models['cube_red'] = model
                elif "green" in model:
                    self.models['cube_green'] = model
                else:
                    continue

    def model_list(self):
	"returns the dictionary containing models keys and names."
        return self.models
